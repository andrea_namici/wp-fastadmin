<?php

define("WP_FA_PLUGIN_NAME",'fastadmin');

define("WP_FA_SESSION_NAME", "wp_fa_admin");

define("WP_FA_LANGUAGES_DOMAIN", "fa");

define("WP_FA_PAGES_SLUG_PREFIX","fa_");


/* E-mail */

define("WP_FA_EMAIL_REPLY_TO", "andrea.namici@gmail.com");

define("WP_FA_EMAIL_SENDER_EMAIL", "andrea.namici@gmail.com");

define("WP_FA_EMAIL_SENDER_NAME", "Fast Admin");

define("WP_FA_EMAIL_SITE_NAME", "Wp Site");

define("WP_FA_EMAIL_DEFAULT_CC","");

define("WP_FA_EMAIL_DEFAULT_BCC","");

/* Mist */

define("WP_FA_UNKNOW_RESULT", "unknow");

/* Wp updates enable/disable with admin notifications */
define("WP_AUTO_UPDATE_CORE",false);

